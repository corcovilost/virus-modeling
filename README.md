# Plotting of COVID-19 data
Ted Corcovilos 2020

***This code is for entertainment purposes only.  No claim is made regarding
accuracy.***

This project fetches data about the COVID-19 pandemic and generates plots
in a Jupyter python notebook.

## Initial setup
You need `conda` and `git` installed, and preferably be running in a `bash`
shell.  After cloning this repository, change to the folder of the repo and run

    conda env create -f environment.yml
    git submodule update --init --recursive --remote
    
The first line sets up a conda environment with the necessary libraries, and 
the second line sets up the submodule repositories for the data sources.

## Fetching data
After everything is setup, you may fetch fresh data by running the script

    updateData.sh